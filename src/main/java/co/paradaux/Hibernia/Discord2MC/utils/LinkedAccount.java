package co.paradaux.Hibernia.Discord2MC.utils;

    // Discord ID
    // Minecraft UUID
    // Minecraft Username
    //

import java.util.UUID;

public class LinkedAccount {

    String id;
    UUID uuid;
    String username;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
