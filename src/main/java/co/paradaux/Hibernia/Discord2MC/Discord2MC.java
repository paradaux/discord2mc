package co.paradaux.Hibernia.Discord2MC;

import co.paradaux.Hibernia.Discord2MC.events.DiscordMessageEventListener;
import net.dv8tion.jda.api.JDA;
import net.dv8tion.jda.api.JDABuilder;
import org.bukkit.plugin.java.JavaPlugin;

import javax.security.auth.login.LoginException;

public final class Discord2MC extends JavaPlugin {

    @Override
    public void onEnable() {
        this.getConfig().options().copyDefaults();
        saveDefaultConfig();

        try {
            JDA client = new JDABuilder(getConfig().getString("token"))
                    .addEventListeners(new DiscordMessageEventListener(this))
                    .build();
        } catch (LoginException e)  {
            e.printStackTrace();
        }

    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }
}
