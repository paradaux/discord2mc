package co.paradaux.Hibernia.Discord2MC.events;

import co.paradaux.Hibernia.Discord2MC.utils.JDAUtils;
import net.dv8tion.jda.api.entities.ChannelType;
import net.dv8tion.jda.api.entities.Role;
import net.dv8tion.jda.api.events.message.MessageReceivedEvent;
import net.dv8tion.jda.api.hooks.ListenerAdapter;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.plugin.Plugin;

import java.util.List;

public class DiscordMessageEventListener extends ListenerAdapter {

    Plugin plugin;
    Role mainrole;
    String role;
    List<String> channels;

    public DiscordMessageEventListener(Plugin plugin) {
        this.plugin = plugin;
        this.channels = plugin.getConfig().getStringList("channels-to-listen-to");

    }

    @Override
    public void onMessageReceived(MessageReceivedEvent event) {

        if (event.getMessage().getContentDisplay().equals("")) {
            return;
        }

        if (event.isFromType(ChannelType.PRIVATE)) {
            return;
        } else if (!channels.contains(event.getChannel().getId())) {
            return;
        } else {
            try {
                Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
                    Bukkit.getScheduler().runTask(plugin, () -> {

                        String username = event.getAuthor().getName(); // Username
                        String discriminator = event.getAuthor().getDiscriminator();
                        String guildname = event.getGuild().getName(); // Guildname
                        String message = event.getMessage().getContentDisplay(); // message
                        String channel = event.getMessage().getChannel().getName();

                        String nickname = event.getMember().getNickname(); // Nickname
                        if (nickname == null) {
                            nickname = "";
                        }

                        mainrole = JDAUtils.getHighestFrom(event.getMember());
                        if (mainrole == null) {
                            role = "";
                        } else {
                            role = mainrole.getName();
                        }

                        String chatMessage = plugin.getConfig().getString("chat-format");
                        chatMessage = ChatColor.translateAlternateColorCodes('&', chatMessage);
                        chatMessage = chatMessage.replace("%username%", username);
                        chatMessage = chatMessage.replace("%discriminator%", discriminator);
                        chatMessage = chatMessage.replace("%nickname%", nickname);
                        chatMessage = chatMessage.replace("%guildname%", guildname);
                        chatMessage = chatMessage.replace("%message%", message);
                        chatMessage = chatMessage.replace("%channel%", channel);
                        chatMessage = chatMessage.replace("%mainrole%", role);

                        Bukkit.broadcastMessage(chatMessage);
                    });
                });
            } catch (Exception e) {}
        }
    }

}
